package com.cor;

import com.cor.utils.BuscarPalabra;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CargarArchivo {

     int R=0;
     int C=0;


    public void buscarCoincidencias(char[][] grid, String word)
    {
        BuscarPalabra buscarPalabra=new BuscarPalabra();
        int cont=0;
        for (int row = 0; row < R; row++) {
            for (int col = 0; col < C; col++) {
                if (buscarPalabra.search2D(grid, row, col, word,R,C)){
                    cont++;
                }
            }
        }
        System.out.println("Palabras encontrada en la sopa de letras: "+cont);
    }

    public  char[][] muestraContenido(String archivo) throws Exception {
        String cadena;
        char [][] sopa=null;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        int cont=0;
        int conFilas=0;
        boolean terminar=false;

        List<String> cadenas=new ArrayList<>();
        while((cadena = b.readLine())!=null) {
            try{
                if(cont==0){
                    String []initial=cadena.split(" ");
                    if(initial[0]!=null && initial[1]!=null){
                        R=Integer.parseInt(initial[0]);
                        C=Integer.parseInt(initial[1]);
                    }
                }else{
                    conFilas++;
                    if(cadena.trim().length()!=C || cadena.trim().length()>100){
                        terminar=true;
                        System.out.println("ERROR, la data no es correcta en las columnas");
                        break;

                    }
                    cadenas.add(cadena.trim());
                }

            }catch (Exception e){
                terminar=true;
                System.out.println("ERROR, Archivo no valido "+e.getMessage());
                break;
            }

            cont++;
        }
        if(conFilas!=R || conFilas>100){
            terminar=true;
            System.out.println("ERROR, la data no es correcta en las filas");
            return null;
        }
        b.close();
        if(!terminar){
            sopa=new char[R][C];

            for(int i=0; i<cadenas.size();i++){

                for(int j=0;j<cadenas.get(i).length();j++){
                    sopa[i][j]=cadenas.get(i).charAt(j);
                }

            }
        }

        return sopa;
    }
}
