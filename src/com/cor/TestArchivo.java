package com.cor;

import org.junit.Assert;
import org.junit.Test;

public class TestArchivo {

    @Test
    public void cargardatos() throws Exception {
        CargarArchivo cargarArchivo=new CargarArchivo();
        char[][] sopa = cargarArchivo.muestraContenido("archivo2.txt");
        if(sopa!=null){
            cargarArchivo.buscarCoincidencias(sopa, "OIE");
            Assert.assertNotNull(sopa);
        }else{
            Assert.assertNull(sopa);
        }

    }

}