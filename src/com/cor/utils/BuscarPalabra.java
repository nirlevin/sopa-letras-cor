package com.cor.utils;

public class BuscarPalabra {

    static int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
    static int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };

    public  boolean search2D(char[][] grid, int row,int col, String word, int R, int C) {

        if (grid[row][col] != word.charAt(0))
            return false;

        int len = word.length();


        for (int dir = 0; dir < 8; dir++) {

            int k, rd = row + x[dir], cd = col + y[dir];

            for (k = 1; k < len; k++) {
                if (rd >= R || rd < 0 || cd >= C || cd < 0)
                    break;

                if (grid[rd][cd] != word.charAt(k))
                    break;

                rd += x[dir];
                cd += y[dir];
            }

            if (k == len)
                return true;
        }
        return false;
    }


}
